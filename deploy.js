var postmark = require('postmark');
var glob = require('glob');
var matter = require('gray-matter');
var mjml = require('mjml');
var key = process.env.POSTMARK_API_KEY
var files = glob.sync('./src/*.mjml');
var client = new postmark.Client(key);
client.getTemplates((error, response) => {
  var templates = response.Templates;
  files.forEach((file) => {
    console.log(`Updating template ${file} ...`);
    var template = matter.read(file);
    var body = {
      Name: template.data.name,
      Subject: template.data.subject,
      TextBody: template.data.txt,
      HtmlBody: mjml.mjml2html(template.content).html
    };
    var index = templates.map(t => t.Name).indexOf(body.Name);
    var id = null;
    if (index >= 0) {
      id = templates.map(t => t.TemplateId)[index];
    }
    if (id) {
      client.editTemplate(id, body, (e, rsp) => console.log(e, rsp));
    } else {
      client.createTemplate(body, (e, rsp) => console.log(e, rsp));
    }
  });
});