var gulp = require('gulp');
var connect = require('gulp-connect');
var mjml = require('gulp-mjml');

gulp.task('connect', () => {
  connect.server({
    livereload: true,
    root: 'build'
  });
});

gulp.task('mjml', () => {
  return gulp.src('src/*.mjml')
    .pipe(mjml())
    .pipe(gulp.dest('build'));
});

gulp.task('html', () => {
  gulp.src('./build/*.html')
    .pipe(connect.reload());
});

gulp.task('watch', () => {
  gulp.watch(['./src/*.mjml', './src/includes/*.mjml'], ['mjml']);
  gulp.watch(['./build/*.html'], ['html']);
})

gulp.task('default', ['connect', 'mjml', 'watch']);
